from django.shortcuts import render

from rest_framework import (
    generics,
    filters,
    mixins,
    status,
    viewsets
)
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Booking
from apps.car.models import Car
from apps.renter.models import Renter
from .serializers import BookingSerializer

# view to create and list
class BookingView(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.DestroyModelMixin,
                  generics.GenericAPIView):
    serializer_class = BookingSerializer

    def get_queryset(self):
        return Booking.objects.select_related(
            'car',
            'renter',
        ).order_by('-created_at')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# view to edit all and partial
class BookingDetail(mixins.DestroyModelMixin, generics.RetrieveUpdateAPIView):
    serializer_class = BookingSerializer

    def get_queryset(self):
        return Booking.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
