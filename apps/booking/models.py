from django.db import models
from apps.car.models import Car
from apps.renter.models import Renter


class Booking(models.Model):
    renter = models.ForeignKey(
        Renter, related_name='renter', on_delete=models.CASCADE, null=True, blank=True
    )
    car = models.ForeignKey(
        Car, related_name='car', on_delete=models.CASCADE, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        db_table = 'booking'
        ordering = ('-id',)

    # def __str__(self):
    #     return self.id
