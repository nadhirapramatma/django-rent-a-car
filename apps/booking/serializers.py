from rest_framework import serializers
from .models import Booking
from apps.car.models import Car
from apps.renter.models import Renter


class BookingSerializer(serializers.ModelSerializer):
    renter_id = serializers.IntegerField(
        source='renter.id',
        required=True
    )
    car_id = serializers.IntegerField(
        source='car.id',
        required=True
    )
    car_brand = serializers.CharField(
        source='car.brand',
        read_only=True
    )
    car_variant = serializers.CharField(
        source='car.variant',
        read_only=True
    )
    car_color = serializers.CharField(
        source='car.get_color_display',
        read_only=True
    )
    car_status = serializers.CharField(
        source='car.get_status_display',
        read_only=True
    )

    def to_representation(self, instance):
        result = super(BookingSerializer,
                       self).to_representation(instance)

        return result

    def create(self, validated_data):
        validated_data['renter'] = get_object_or_404(
            Renter,
            pk=validated_data['renter']['id']
        )
        validated_data['car'] = get_object_or_404(
            Car,
            pk=validated_data['car']['id']
        )

        return Booking.objects.create(**validated_data)

    def update(self, instance, validated_data):
        if 'renter' in validated_data:
            instance.renter = get_object_or_404(
                Renter,
                pk=validated_data['renter']['id']
            )
            del validated_data['renter']
        if 'car' in validated_data:
            instance.car = get_object_or_404(
                Car,
                pk=validated_data['car']['id']
            )
            del validated_data['car']

        super(self.__class__, self).update(instance, validated_data)
        updated = Car.objects.get(id=instance.id)
        return updated

    class Meta:
        model = Booking
        fields = (
            'renter_id', 'car_id', 'car_brand', 'car_variant', 'car_color', 'car_status', 'created_at', 'updated_at'
        )
