from django.conf.urls import url

from apps.booking import views

# define urls
urlpatterns = [
    url(
        r'^$', views.BookingView.as_view(),
        name='get-post-booking'
    ),
    url(
        r'^(?P<pk>[0-9]+)/$',
        views.BookingDetail.as_view(),
        name="retrieve-delete-booking"
    ),
]
