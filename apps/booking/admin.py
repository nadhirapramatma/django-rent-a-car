from django.contrib import admin

# Register model
from .models import Booking


# set field to be shown for admin
@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'renter',
        'car',
        'created_at',
        'updated_at'
    )
