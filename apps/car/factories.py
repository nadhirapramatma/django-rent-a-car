import factory
from apps.car.models import (
    Car,
)


# define factory used in tests constructor

class CarFactory(factory.DjangoModelFactory):
    id = factory.Sequence(lambda n: n + 1)

    class Meta:
        model = Car
