# create constant tuples for choice field

COLOR_CHOICES = (
    ('1', 'Black'),
    ('2', 'Red'),
    ('3', 'White'),
    ('4', 'Silver'),
    ('5', 'Grey'),
    ('6', 'Green'),
)

STATUS_CHOICES = (
    ('1', 'Free'),
    ('2', 'Rented'),
)
