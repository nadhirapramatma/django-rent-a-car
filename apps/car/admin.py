from django.contrib import admin

# Register model
from .models import Car


# set field to be shown for admin
@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'brand',
        'variant',
        'color',
        'status',
        'created_at',
        'updated_at'
    )
