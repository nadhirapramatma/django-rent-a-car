from rest_framework import serializers
from .models import Car
from apps.renter.models import Renter
from .constants import *


class CarSerializer(serializers.ModelSerializer):
    brand = serializers.CharField(required=True)
    variant = serializers.CharField(required=True)
    color = serializers.ChoiceField(
        choices=COLOR_CHOICES,
        source='get_color_display',
        required=True
    )
    status = serializers.ChoiceField(
        choices=STATUS_CHOICES,
        source='get_status_display',
        required=True
    )

    def create(self, validated_data):
        validated_data['brand'] = validated_data.get('brand')
        validated_data['variant'] = validated_data.get('variant')
        if 'get_color_display' in validated_data:
            validated_data['color'] = validated_data['get_color_display']
            del validated_data['get_color_display']
        if 'get_status_display' in validated_data:
            validated_data['status'] = validated_data['get_status_display']
            del validated_data['get_status_display']

        return Car.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.brand = validated_data.get('title', instance.brand)
        instance.variant = validated_data.get(
            'variant', instance.variant)
        instance.color = validated_data.get(
            'get_color_display', instance.color)
        instance.status = validated_data.get(
            'get_status_display', instance.status)

        instance.save()
        return instance

    class Meta:
        model = Car
        fields = (
            'brand', 'variant', 'color', 'status', 'created_at', 'updated_at'
        )
