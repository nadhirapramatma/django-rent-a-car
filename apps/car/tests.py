from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User as CoreUser
from rest_framework.test import APIClient
from apps.car.factories import CarFactory
import datetime
import json
import pytz
from unittest import mock


class CarTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = CoreUser.objects.create_superuser(
            'admin', 'admin@admin.com', 'admin123'
        )
        self.mocked_datetime = datetime.datetime(
            2019, 7, 7, 0, 0, 0,
            tzinfo=pytz.utc
        )
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=self.mocked_datetime)):  # noqa E501
            self.car_1 = CarFactory.create(
                id=1, brand='Suzuki', variant='Ertiga', color=1, status=1)

    def test_get_car(self):
        response = self.client.get(reverse("get-post-car"))
        self.maxDiff = None
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            [
                {
                    "id": self.car_1.id,
                    "brand": self.car_1.brand,
                    "variant": self.car_1.variant,
                    "color": self.car_1.color,
                    "status": self.car_1.status,
                    "created_at": "2019-07-07T00:00:00Z",
                    "updated_at": "2019-07-07T00:00:00Z"
                }
            ]
        )
