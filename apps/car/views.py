from django.shortcuts import render

from rest_framework import (
    generics,
    filters,
    mixins,
    status,
    viewsets
)
from django_filters import (
    CharFilter,
    ChoiceFilter,
    DateFilter,
    FilterSet,
    NumberFilter,
    OrderingFilter,
    Filter
)
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from .models import Car
from apps.renter.models import Renter
from .serializers import CarSerializer
from .constants import *


class CarFilterSet(FilterSet):
    brand = CharFilter(
        field_name='brand', lookup_expr='icontains'
    )
    variant = CharFilter(
        field_name='variant', lookup_expr='icontains'
    )
    color = CharFilter(
        method='filter_color'
    )
    status = CharFilter(
        method='filter_status'
    )

    ordering = OrderingFilter(
        fields=(
            'id', 'brand', 'color', 'created_at'
        )
    )

    def filter_color(self, queryset, name, value):
        if value:
            value = value.lower().title()
            car_dict = {y: x for x, y in COLOR_CHOICES}
            key = car_dict[value]
            list_car_by_color = queryset.filter(
                color=key
            )
            return list_car_by_color

    def filter_status(self, queryset, name, value):
        if value:
            value = value.lower().title()
            car_dict = {y: x for x, y in STATUS_CHOICES}
            key = car_dict[value]
            list_car_by_status = queryset.filter(
                status=key
            )
            return list_car_by_status

    class Meta:
        model = Car
        fields = (
            'brand', 'variant', 'color', 'status'
        )


class CarView(mixins.ListModelMixin,
              mixins.CreateModelMixin,
              mixins.DestroyModelMixin,
              generics.GenericAPIView):
    serializer_class = CarSerializer
    filter_class = CarFilterSet

    def get_queryset(self):
        return Car.objects.order_by('-created_at')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# view to edit all and partial
class CarDetail(mixins.DestroyModelMixin, generics.RetrieveUpdateAPIView):
    serializer_class = CarSerializer

    def get_queryset(self):
        return Car.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
