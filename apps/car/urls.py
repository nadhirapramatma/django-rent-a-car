from django.conf.urls import url

from apps.car import views

# define urls
urlpatterns = [
    url(
        r'^$', views.CarView.as_view(),
        name='get-post-car'
    ),
    url(
        r'^(?P<pk>[0-9]+)/$',
        views.CarDetail.as_view(),
        name="retrieve-delete-car"
    ),
]
