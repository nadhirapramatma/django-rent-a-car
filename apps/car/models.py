from django.db import models
from apps.renter.models import Renter
from .constants import *


class Car(models.Model):
    brand = models.CharField(max_length=32, null=True)
    variant = models.CharField(max_length=50, null=True)
    color = models.IntegerField(choices=COLOR_CHOICES, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        db_table = 'car'
        ordering = ('-id',)

    def __str__(self):
        return self.brand
