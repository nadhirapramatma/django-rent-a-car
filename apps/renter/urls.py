from django.conf.urls import url

from apps.renter import views

# define urls
urlpatterns = [
    url(
        r'^$', views.RenterView.as_view(),
        name='get-post-renter'
    ),
    url(
        r'^(?P<pk>[0-9]+)/$',
        views.RenterDetail.as_view(),
        name="retrieve-delete-renter"
    ),
]
