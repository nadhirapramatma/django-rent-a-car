from django.shortcuts import render
from rest_framework import (
    generics,
    filters,
    mixins,
    status,
    viewsets
)
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Renter
from .serializers import RenterSerializer


class RenterView(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 mixins.DestroyModelMixin,
                 generics.GenericAPIView):
    serializer_class = RenterSerializer

    def get_queryset(self):
        return Renter.objects.order_by('-created_at')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# view to edit all and partial
class RenterDetail(mixins.DestroyModelMixin, generics.RetrieveUpdateAPIView):
    serializer_class = RenterSerializer

    def get_queryset(self):
        return Renter.objects.all()

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
