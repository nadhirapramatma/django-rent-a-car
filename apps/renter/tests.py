from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User as CoreUser
from rest_framework.test import APIClient
from apps.renter.factories import RenterFactory
import datetime
import pytz
from unittest import mock


class RenterTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = CoreUser.objects.create_superuser(
            'admin', 'admin@admin.com', 'admin123'
        )
        self.mocked_datetime = datetime.datetime(
            2019, 7, 7, 0, 0, 0,
            tzinfo=pytz.utc
        )
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=self.mocked_datetime)):  # noqa E501
            self.renter_1 = RenterFactory.create(
                id='1', name='susi', email='susi@gmail.com')
            self.renter_2 = RenterFactory.create(
                id='2', name='alan', email='alan@gmail.com')
            self.renter_3 = RenterFactory.create(
                id='3', name='jojo', email='jojo@gmail.com')

    def test_get_renter(self):
        response = self.client.get(reverse("get-post-renter"))
        self.maxDiff = None
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            [
                {
                    "id": self.renter_1.id,
                    "name": self.renter_1.name,
                    "email": "susi@gmail.com",
                    "created_at": "2019-07-07T00:00:00Z",
                    "updated_at": "2019-07-07T00:00:00Z"
                },
                {
                    "id": self.renter_2.id,
                    "name": self.renter_2.name,
                    "email": "alan@gmail.com",
                    "created_at": "2019-07-07T00:00:00Z",
                    "updated_at": "2019-07-07T00:00:00Z"
                },
                {
                    "id": self.renter_3.id,
                    "name": self.renter_3.name,
                    "email": "jojo@gmail.com",
                    "created_at": "2019-07-07T00:00:00Z",
                    "updated_at": "2019-07-07T00:00:00Z"
                }
            ]
        )
