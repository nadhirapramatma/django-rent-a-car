from rest_framework import serializers
from .models import Renter


class RenterSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    email = serializers.CharField(required=True)

    def create(self, validated_data):
        return Renter.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.email = validated_data.get(
            'email', instance.email)

        instance.save()
        return instance

    class Meta:
        model = Renter
        fields = (
            'id', 'name', 'email', 'created_at', 'updated_at'
        )
