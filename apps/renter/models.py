from django.db import models


class Renter(models.Model):
    name = models.CharField(max_length=32, null=True)
    email = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        db_table = 'renter'
        ordering = ('-id',)

    def __str__(self):
        return self.name
