import factory
from apps.renter.models import (
    Renter,
)


# define factory used in tests constructor

class RenterFactory(factory.DjangoModelFactory):
    id = factory.Sequence(lambda n: n + 1)
    name = 'susi'
    email = 'susi@gmail.com'

    class Meta:
        model = Renter
