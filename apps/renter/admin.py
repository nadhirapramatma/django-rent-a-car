from django.contrib import admin

# Register model
from .models import Renter


# set field to be shown for admin
@admin.register(Renter)
class RenterAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'email',
        'created_at',
        'updated_at'
    )
