#  Rent A Car Web Service

>This application is made using [Django](https://www.djangoproject.com/) (a high-level Python Web framework that encourages rapid development and clean, pragmatic design) and [Django REST Framework](http://www.django-rest-framework.org/) (a powerful and flexible toolkit for building Web APIs)

This web service provides method to create, read, update, and delete entities, including:
- Renter (person who can rent a car)
- Car (car provided by admin)
- Booking (booking data)

## How To Use

- Install python
Being a Python Web framework, Django requires [Python 3.6](https://www.python.org/downloads/). [See What Python version can I use with Django?](https://docs.djangoproject.com/en/2.0/faq/install/#faq-python-version-support) for details. We recommend using **python v3.6**

You can verify that Python is installed by typing `python` from your shell; you should see something like:

```
Python 3.6.x
[GCC 4.x] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

- Create your working environment
- Install dependences using pip.
- Download this project.
- Run python manage.py runserver localhost:8000 to run server
- Run python manage.py test apps/*/tests.py to test

### Feature
open swagger: http:/localhost:8000/swagger/
- To create, read RENTER, use endpoint: /renter/
- To edit, and delete RENTER, use endpoint: /renter/{id} 
- To create, read CAR, use endpoint: /car/
- To edit, and delete CAR, use endpoint: /car/{id}
- To create, read BOOKING, use endpoint: /car/
- To edit, and delete BOOKING, use endpoint: /car/{id}
- To filter CAR by status, use endpoint: /car/ then input parameter of status (input: free or rented)
- To filter CAR by color, use endpoint: /car/ then input parameter of color (input whatever color provided by COLOR_CHOICES)
- To filter CAR by brand and variant, input parameter of brand and variant

### Django Admin
- run: python manage.py createsuperuser, fill the form
- open django admin: http:/localhost:8000/admin/